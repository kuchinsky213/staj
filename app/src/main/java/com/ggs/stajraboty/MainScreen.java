package com.ggs.stajraboty;

import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Calendar;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainScreen extends AppCompatActivity   {
    //----------------------------------------------------------DATE PICKER 1
    String date1;
    public static final String DATE_DIALOG_1 = "datePicker1";

    private static int mYear1;
    private static int mMonth1;
    private static int mDay1;
    private static int staj_y, staj_m, staj_d;
    //----------------------------------------------------------DATE PICKER 1


    //----------------------------------------------------------DATE PICKER 2
    String date2;
    public static final String DATE_DIALOG_2 = "datePicker2";

    private static int mYear2;
    private static int mMonth2;
    private static int mDay2;
    Button priem, uvolnenie, rasswet;
 static TextView u, p, resultat;
    //----------------------------------------------------------DATE PICKER 2
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        p=findViewById(R.id.textView3);
        u=findViewById(R.id.textView5);
        //----------------------------------------------------------DATE PICKER 1
        priem = findViewById(R.id.button);
        priem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DialogFragment newFragment1 = new DatePickerFragment1();
                newFragment1.show(getSupportFragmentManager(), DATE_DIALOG_1);
            }
        });
        //----------------------------------------------------------DATE PICKER 1


        //----------------------------------------------------------DATE PICKER 2
        uvolnenie = findViewById(R.id.button2);
        resultat = findViewById(R.id.resultat);
        rasswet = findViewById(R.id.button3);
        uvolnenie.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DialogFragment newFragment2 = new DatePickerFragment2();
                newFragment2.show(getSupportFragmentManager(), DATE_DIALOG_2);
                u.setVisibility(View.VISIBLE);
            }
        });
        //----------------------------------------------------------DATE PICKER 2




        rasswet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
staj_y=mYear2-mYear1;
staj_m=mMonth2-mMonth1;
staj_d=mDay2-mDay1;
//if (staj_y==1){
//    resultat.setText(staj_y+" год "+staj_m+ " месяцев "+ staj_d+" дней");
//} else if (staj_y>1&&staj_y<5){
//    resultat.setText(staj_y+" годa "+staj_m+ " месяцев "+ staj_d+" дней");
//} else if (staj_y>4){
//    resultat.setText(staj_y+" лет "+staj_m+ " месяцев "+ staj_d+" дней");
//} else if (staj_y==0){
//    resultat.setText(staj_m+ " месяцев "+ staj_d+" дней"); }

                try {
                    //Dates to compare
                    String CurrentDate=  mMonth2+"/"+ mDay2 + "/" +mYear2;
                    String FinalDate=   mMonth1+"/"+ mDay1 + "/" +mYear1;

                    Date date1;
                    Date date2;

                    SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");

                    //Setting dates
                    date1 = dates.parse(CurrentDate);
                    date2 = dates.parse(FinalDate);

                    //Comparing dates
                    long difference = Math.abs(date1.getTime() - date2.getTime());
                    long differenceDates = difference / (24 * 60 * 60 * 1000);
                //    long diffInYears = ChronoUnit.YEARS.between((Temporal) date1, (Temporal) date2);
              //      long diffInWeeks = ChronoUnit.WEEKS.between(date1.toInstant(), date2.toInstant());
                    //Convert long to String
                    String dayDifference = Long.toString(differenceDates);
                    resultat.setText(dayDifference+" дней" );
                    Log.e("HERE","HERE: " + dayDifference);

                } catch (Exception exception) {
                    Log.e("DIDN'T WORK", "exception " + exception);
                }
//
//                LocalDate last = LocalDate.of(mYear1, mMonth1+1, mDay1);
//                LocalDate first = LocalDate.of(mYear2, mMonth2+1, mDay2);
//                LocalDate resultDate = compare(last, first); //returns 0011-08-25
//
//                resultat.setText(resultDate+"");
            }
        });



    }
    LocalDate compare(LocalDate dateOfSession, LocalDate compared)
    {
        long resultDays = ChronoUnit.DAYS.between(compared, dateOfSession);
        return LocalDate.of(0, 1, 1).plusDays(resultDays - 1);
    }
    //----------------------------------------------------------DATE PICKER 1
    public static class DatePickerFragment1 extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            Locale locale = new Locale("RU"); Locale.setDefault(locale); Configuration config = new Configuration(); config.locale = locale; getActivity().getResources().updateConfiguration(config, null);
            //Date Time NOW
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(),R.style.DatePicker1, this, year, month, day);
        }
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // get selected date
            mYear1 = year;
            mMonth1 = month;
            mDay1 = day;
            // show selected date to date button
            p.setText(new StringBuilder()
                    .append(mYear1).append(" год ")
                    .append(mMonth1 + 1).append(" месяц ")
                    .append(mDay1).append(" число "));
        }
    }
    //----------------------------------------------------------DATE PICKER 1


    //----------------------------------------------------------DATE PICKER 2
    public static class DatePickerFragment2 extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // set default date
            Locale locale = new Locale("RU"); Locale.setDefault(locale); Configuration config = new Configuration(); config.locale = locale; getActivity().getResources().updateConfiguration(config, null);

            //Date Time FROM BEFORE
            final Calendar a = Calendar.getInstance();
            int year = a.get(Calendar.YEAR);
            int month = a.get(Calendar.MONTH);
            int day = a.get(Calendar.DAY_OF_MONTH);


            return new DatePickerDialog(getActivity(),R.style.DatePicker2, this, year, month, day);
        }
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // get selected date
            mYear2 = year;
            mMonth2 = month;
            mDay2 = day;
            // show selected date to date button
            u.setText(new StringBuilder()
                    .append(mYear2).append(" год ")
                    .append(mMonth2 + 1).append(" месяц ")
                    .append(mDay2).append(" число"));
        }
    }
    //----------------------------------------------------------DATE PICKER 2

}
