package com.ggs.stajraboty;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGHT = 4500;
    ImageView logo;
    Animation vrawaemsya;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        logo=findViewById(R.id.logo);

        vrawaemsya = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rotate);
        logo.startAnimation(vrawaemsya);
        new Handler().postDelayed(new Runnable() {

            @Override

            public void run() {

                Intent mainIntent = new Intent(MainActivity.this, MainScreen.class);

                MainActivity.this.startActivity(mainIntent);

                MainActivity.this.finish();

            }

        }, SPLASH_DISPLAY_LENGHT);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}